# IE Merger
A python script used to merge the data coming from the Information Extraction (IE) task.

## How-To

### Run
The script is released as Docker image that can be found in the private [container registry page](https://gitlab.unige.ch/addmin/ie-merger/container_registry/160) of the project.

The script is using both Kafka and RabbitMQ, thus a running instance of each is required. The endpoints are configurables using the following environment variables:

`RABBITMQ_HOST`: address of the RabbitMQ host (default: `rabbitmq`)

`RABBITMQ_PORT`: port the RabbitMQ host (default: `49153`)

`EVENTS_QUEUE`: RabbitMQ queue where the events are posted (default: `events`)

`KAFKA_HOST`: address of the Kafka host (default: `kafka`)

`KAFKA_PORT`: port of the Kafka host (default: `9092`)

`TOPIC`: Kafka topic to be used (default: `rml-streamer-in`)

#### Example with default env vars

`docker run registry.gitlab.unige.ch/addmin/ie-merger:latest`

#### Example with inline env vars

`docker run --env RABBITMQ_HOST=rabbitmq_host --env EVENTS_QUEUE=new_events registry.gitlab.unige.ch/addmin/ie-merger:latest`

**NB:** the variables not set will use the default value

#### Example with env vars passed using a `.env` file

`docker run --env-file .env registry.gitlab.unige.ch/addmin/ie-merger:latest`

**NB:** you can use the [`.env.sample`](https://gitlab.unige.ch/addmin/ie-merger/-/raw/master/.env.sample) as starting point.
