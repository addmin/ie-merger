let publishCmd = `
echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY --username $CI_REGISTRY_USER --password-stdin
docker build -t "$CI_REGISTRY_IMAGE:\${nextRelease.version}" -t "$CI_REGISTRY_IMAGE:latest" .
docker push --all-tags "$CI_REGISTRY_IMAGE"
`
let config = require('semantic-release-preconfigured-conventional-commits');
config.plugins.push(
    [
        "@semantic-release/exec",
        {
            "publishCmd": publishCmd
        }
    ],
    "@semantic-release/gitlab",
    "@semantic-release/git"
)
module.exports = config