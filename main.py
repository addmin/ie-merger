import argparse
import logging
import os
import sys

import pika
from dotenv import load_dotenv, find_dotenv
from kafka import KafkaProducer

import json

merged = {"extractedFields": []}
messages_read = 0
n_messages = 0

config = {}


def get_env_var(var_name):
    if os.environ.get(var_name) is None:
        logging.error(f"{var_name} value is missing")
        sys.exit(0)
    return os.environ.get(var_name)


def init():
    if find_dotenv():
        load_dotenv()

    if len(sys.argv) > 1:
        read_params()
    else:
        config["RABBITMQ_HOST"] = get_env_var("RABBITMQ_HOST")
        config["RABBITMQ_PORT"] = get_env_var("RABBITMQ_PORT")
        config["EVENTS_QUEUE"] = get_env_var("EVENTS_QUEUE")

        config["KAFKA_HOST"] = get_env_var("KAFKA_HOST")
        config["KAFKA_PORT"] = get_env_var("KAFKA_PORT")
        config["TOPIC"] = get_env_var("TOPIC")

    logging.getLogger().setLevel(logging.INFO)


def read_params():
    parser = argparse.ArgumentParser()
    parser.add_argument("-rh", "--rabbit_host")
    parser.add_argument("-rp", "--rabbit_port")
    parser.add_argument("-kh", "--kafka_host")
    parser.add_argument("-kp", "--kafka_port")
    args = parser.parse_args()
    if args.rabbit_host is not None:
        config["RABBITMQ_HOST"] = args.rabbit_host
    if args.rabbit_port is not None:
        config["RABBITMQ_PORT"] = args.rabbit_port
    if args.kafka_host is not None:
        config["KAFKA_HOST"] = args.kafka_host
    if args.kafka_port is not None:
        config["KAFKA_PORT"] = args.kafka_port


def kafka_producer(data):
    producer = KafkaProducer(bootstrap_servers=f"{config['KAFKA_HOST']}:{config['KAFKA_PORT']}",
                             value_serializer=lambda v: json.dumps(v).encode("utf-8"))
    producer.send(config["TOPIC"], data)


def merge_data(ch, method, properties, body):
    global messages_read, n_messages, merged
    data = json.loads(body)
    if "class" not in merged.keys():
        merged["class"] = data["class"]
    for el in data["extractedFields"]:
        merged["extractedFields"].append(el)
    messages_read += 1
    logging.info(f"Read {messages_read} messages over {n_messages}")
    if messages_read == n_messages:
        logging.info(str(merged))
        kafka_producer(merged)
        messages_read = 0
        merged = {"extractedFields": []}
        ch.queue_delete(queue=data["id"])


def main(ch):
    ch.queue_declare(queue=config["EVENTS_QUEUE"])

    def process_data(queue):
        q = ch.queue_declare(queue=queue)
        global n_messages
        n_messages = q.method.message_count
        if n_messages != 0:
            ch.basic_consume(queue=queue, auto_ack=True, on_message_callback=merge_data)

    def on_event_message(c, method, properties, body):
        event = json.loads(body)
        logging.info(f"[x] Received {event}")
        if event["name"] == "write_end":
            merged["id"] = event["on"]
            process_data(event["on"])

    ch.basic_consume(queue=config["EVENTS_QUEUE"], auto_ack=True, on_message_callback=on_event_message)
    logging.info("[*] Waiting for messages. To exit press CTRL+C")
    ch.start_consuming()


if __name__ == "__main__":
    try:
        init()
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=config["RABBITMQ_HOST"], port=config["RABBITMQ_PORT"]))
        channel = connection.channel()
        main(channel)
    except KeyboardInterrupt:
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
