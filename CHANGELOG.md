## [1.0.1](https://gitlab.unige.ch/addmin/ie-merger/compare/1.0.0...1.0.1) (2022-11-28)


### General maintenance

* add README ([4f38db2](https://gitlab.unige.ch/addmin/ie-merger/commit/4f38db220a60ae9dab48a85690cc754beb1d8d01))
* update README ([adfc646](https://gitlab.unige.ch/addmin/ie-merger/commit/adfc6464e4fa6e9684d784da9ff8f5a44956ccd6))


### Build and continuous integration

* **deps:** update dependency @semantic-release/gitlab to v9 ([144cc63](https://gitlab.unige.ch/addmin/ie-merger/commit/144cc6316d204ed33491cce7656246cc81985480))
* **deps:** update dependency @semantic-release/gitlab to v9.1.3 ([ce4141e](https://gitlab.unige.ch/addmin/ie-merger/commit/ce4141e9db1bdd123a32dee686e21ef279f24894))
* **deps:** update dependency @semantic-release/gitlab to v9.2.0 ([96638df](https://gitlab.unige.ch/addmin/ie-merger/commit/96638dff37554a703ef74b0d54d0012932311163))
* **deps:** update dependency @semantic-release/gitlab to v9.2.1 ([6564ca2](https://gitlab.unige.ch/addmin/ie-merger/commit/6564ca2180b8563ba89e7a478538b79b9ec60747))
* **deps:** update docker docker tag to v20.10.21 ([91ff02f](https://gitlab.unige.ch/addmin/ie-merger/commit/91ff02f4592444005ce08811ada0220f78b4b677))
* **deps:** update node.js to 16.15 ([abd8e41](https://gitlab.unige.ch/addmin/ie-merger/commit/abd8e418809a343b296ae61ad8c8a6d75d52dcba))


### Dependency updates

* **core-deps:** update python docker tag to v3.11 ([338637a](https://gitlab.unige.ch/addmin/ie-merger/commit/338637ae346d51129777dab880a17091592d2e74))

## 1.0.0 (2022-04-21)


### General maintenance

* add check on env vars ([d9ea154](https://gitlab.unige.ch/addmin/ie-merger/commit/d9ea154307ad05debb9060e428b17cb512794127))
* **dockerfile:** use the .env.sample as default config within the docker image ([c2dad41](https://gitlab.unige.ch/addmin/ie-merger/commit/c2dad41aeb82550467d8c418933ca3fef853ed49))
* **dockerignore:** add renovate.json ([30d05da](https://gitlab.unige.ch/addmin/ie-merger/commit/30d05da7a061875071cc29d9c1c7d750a73f10ac))
* update renovate config ([75f7fbb](https://gitlab.unige.ch/addmin/ie-merger/commit/75f7fbb1fb0eff3ae35f642344b2bac103b42da0))


### Build and continuous integration

* add package.json for semantic-release ([b55dbc5](https://gitlab.unige.ch/addmin/ie-merger/commit/b55dbc53b018dedffba71e449f65d97c0e141da0))
* add semantic-release config ([9504f7e](https://gitlab.unige.ch/addmin/ie-merger/commit/9504f7e036fb63177e99cc3c81a0513bbe87253a))
* **deps:** pin dependency @semantic-release/gitlab to v8.1.0 ([76f6d80](https://gitlab.unige.ch/addmin/ie-merger/commit/76f6d8021784d47ac3be94f73c88b68e4722c276))
* update with semantic-release ([23f7f07](https://gitlab.unige.ch/addmin/ie-merger/commit/23f7f07c406fdd77ca25f1a3cf9639bc06bcf50b))


### Dependency updates

* **core-deps:** update python docker tag to v3.10 ([db4c748](https://gitlab.unige.ch/addmin/ie-merger/commit/db4c7482bd55758358447649b31a09467496324f))
* **deps:** add renovate.json ([f6436b6](https://gitlab.unige.ch/addmin/ie-merger/commit/f6436b62892e7df64fef895c159d9a45404b5a5f))
