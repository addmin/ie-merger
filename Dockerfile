FROM python:3.11-slim

WORKDIR /app

LABEL Maintainer="Ashley.Caselli"

COPY Pipfile* ./
COPY .env.sample ./.env

RUN pip install pipenv && \
  apt-get update && \
  apt-get install -y --no-install-recommends gcc python3-dev libssl-dev && \
  pipenv install --deploy --system && \
  apt-get remove -y gcc python3-dev libssl-dev && \
  apt-get autoremove -y && \
  pip uninstall pipenv -y

COPY ./ ./

CMD ["python", "main.py"]